<div id="content">
	<div id="welcome">
		<div class="row">
			<div class="flex-container">
				<img src="public/images/content/img1.jpg" alt="tree shadow" class="mbImage img1">
				<div class="wlcRight margin0">
					<h1>Robert's <span>Complete Care</span></h1>
					<div class="box">
						<div class="text">
							<p>Robert’s Complete Care has a proven track record of providing creative, high quality gardening services in the Whittier, CA area. Our excellent lawn care, tree trimming, deck building and mulching services have brought many smiles to our clients over the years. Homeowners who have had the pleasure of working with our professional staff find that we not only produce a better landscape environment, but our services often save them money in the long run. </p>
							<a href="about#content" class="btn">LEARN MORE</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="tree-services">
		<div class="row">
			<div class="flex-container">
				<div class="tsLeft margin0">
					<p class="phone none">
						CALL US TODAY
						<span><a href="tel:<?php $this->info("phone"); ?>"><?php $this->info("phone-display"); ?></a></span>
					</p>
					<div class="container">
						<h2>Tree Services</h2>
						<div class="box">
							<div class="text">
								<p>No one can deny that Whittier has natural beauty without equal. This is what attracts so many visitors and new residents to the state. Our tree removal service will help you make the most of your property by shaping the landscape to your liking. Our landscaping company includes the full gamut of tree services options. Our experts can inspect trees for diseases. Just as with humans, sickness can spread from tree to tree, making it important to fix the problem as soon as it arises. In cases where you want the tree to stay, some simple tree trimming may be sufficient.</p>
								<a href="services#content" class="btn">LEARN MORE</a>
							</div>
						</div>
					</div>
				</div>
				<img src="public/images/content/img2.jpg" alt="tree shadow" class="img2 mbImage">
			</div>
		</div>
	</div>
	<div id="services">
		<div class="row">
			<h2>Our Services</h2>
			<div class="container">
				<div class="flex-container">
					<dl>
						<dt><img src="public/images/content/service1.jpg" alt="Services Image"></dt>
						<dd><span>MASONRY</span></dd>
					</dl>
					<dl>
						<dt><img src="public/images/content/service2.jpg" alt="Services Image"></dt>
						<dd><span>PAVING</span></dd>
					</dl>
					<dl>
						<dt><img src="public/images/content/service3.jpg" alt="Services Image"></dt>
						<dd><span>SYNTHETIC <br/> LAWNS</span></dd>
					</dl>
					<dl>
						<dt><img src="public/images/content/service4.jpg" alt="Services Image"></dt>
						<dd><span>LIGHT <br/> PLUMBING</span></dd>
					</dl>
				</div>
				<p class="info1">A LANDSCAPER FROM OUR PROFESSIONAL STAFF WILL BE <br> ON SITE TO ENSURE A JOB WELL DONE. </p>
				<p class="info2">To learn more about our landscape and tree services in Whittier, CA, call us at:</p>
				<p class="phone"><a href="tel:<?php $this->info("phone"); ?>"><?php $this->info("phone-display"); ?></a></p>
				<a href="services#content" class="btn">LEARN MORE</a>
			</div>
		</div>
	</div>
	<div id="contact">
		<div class="row">
			<div class="flex-container">
				<div class="cntLeft margin0">
					<div class="container">
						<h2>Contact Us</h2>
						<div class="box">
							<h3>GET A FREE QUOTE</h3>
							<h4>OR CALL US AT (562) 208-0148</h4>
							<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
								<div class="top-form flex-container">
									<label><span class="ctc-hide">Name:</span>
										<input type="text" name="name" placeholder="">
									</label>
									<label><span class="ctc-hide">Email:</span>
										<input type="text" name="email" placeholder="">
									</label>
									<label><span class="ctc-hide">Phone:</span>
										<input type="text" name="phone" placeholder="">
									</label>
									<label><span class="ctc-hide">Address:</span>
										<input type="text" name="address" placeholder="">
									</label>
								</div>
								<label><span class="ctc-hide">Message:</span>
									<textarea name="message" cols="30" rows="10" placeholder=""></textarea>
								</label>
								<div class="ckbot">
									<label for="g-recaptcha-response"><span class="ctc-hide g">Recaptcha</span></label>
									<div class="g-recaptcha"></div>
									<label>
										<input type="checkbox" name="consent" class="consentBox">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.
									</label><br>
									<?php if( $this->siteInfo['policy_link'] ): ?>
									<label>
										<input type="checkbox" name="termsConditions" class="termsBox"/> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a>
									</label>
									<?php endif ?>
								</div>
								<button type="submit" class="ctcBtn btn" disabled>SUBMIT FORM</button>
							</form>
						</div>
					</div>
				</div>
				<img src="public/images/content/img3.jpg" alt="tree shadow" class="img2 mbImage">
			</div>
		</div>
	</div>
	<div id="portfolio">
		<div class="row">
			<div class="flex-container">
				<div class="pLeft margin0">
					<img src="public/images/content/portfolio1.jpg" alt="Portfolio Image">
					<img src="public/images/content/portfolio2.jpg" alt="Portfolio Image">
					<img src="public/images/content/portfolio3.jpg" alt="Portfolio Image">
					<img src="public/images/content/portfolio4.jpg" alt="Portfolio Image">
					<img src="public/images/content/portfolio5.jpg" alt="Portfolio Image">
					<img src="public/images/content/portfolio6.jpg" alt="Portfolio Image">
				</div>
				<div class="pRight margin0">
					<h2>Portfolio</h2>
					<div class="box">
						<div class="text">
							<p>Robert’s Complete Care has a proven track record of providing creative, high quality gardening services in the Whittier, CA area. Our emphasis is on consistent quality, but we also appreciate the significant twists and turns that go along with creating a beautiful environment at each unique location. We work with our clients from start to finish.</p>
							<a class="btn" href="gallery#content">VIEW MORE</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="testimonials">
		<div class="row">
			<div class="flex-container margin0">
				<div class="tLeft margin0">
					<h2 class="margin0">Testimonials</h2>
					<div class="box">
						<div class="testimonial">
							<img src="public/images/content/testi1.jpg" alt="A.." class="none">
							<img src="public/images/content/quote.png" alt="quote">
							<p> Reasonable prices for amazing work & also he responds quick! I use to have a gardener and I had so many problems with him but when I switched over to Robert i haven’t had any problems. My husband and I wanted to do our front lawn a certain way and I showed him a picture of how I wanted it to look like and I have never been so amazed by such great work. Would recommend 100% <span>&#9733; &#9733; &#9733; &#9733; &#9733;</span> <span>-A..</span> </p>
						</div>
						<div class="testimonial">
							<img src="public/images/content/testi2.jpg" alt="Shan N." class="none">
							<img src="public/images/content/quote.png" alt="quote">
							<p> We called late Friday morning and he was here by 5pm the same day to do an estimate. Our yard looked like a jungle and had a lot of cleaning up, pruning, and trimming that was way overdue. He and his team did a great job. The front hedges were trimmed well and evenly and he cleaned out dead plants and vines from two flowerbeds and also sprayed the weeds and checked our sprinklers. He was here for almost 3 hours and charged $250. Regular mowing is going to be about $65 for twice a month. He was professional, on time, and really did a great job. <span>&#9733; &#9733; &#9733; &#9733; &#9733;</span> <span>-Shan N.</span> </p>
						</div>
						<div class="testimonial">
							<a class="btn" href="reviews#content">READ MORE</a>
						</div>
					</div>
				</div>
				<div class="tRight margin0 none">
					<img src="public/images/content/testimonial.jpg" alt="House">
				</div>
			</div>
		</div>
	</div>
</div>
