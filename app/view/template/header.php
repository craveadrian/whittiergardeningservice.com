<?php $this->suspensionRedirect($view); ?>
<!DOCTYPE html>
<html lang="en" <?php $this->helpers->htmlClasses(); ?>>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />

	<?php $this->helpers->seo($view); ?>
	<link rel="icon" href="public/images/favicon.png" type="image/x-icon">
	<!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
	<link href="<?php echo URL; ?>public/styles/style.css" rel="stylesheet">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
	<link rel="stylesheet" href="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.css" media="screen" />
	<?php $this->helpers->analytics(); ?>
</head>

<body <?php $this->helpers->bodyClasses($view); ?>>
<?php $this->checkSuspensionHeader(); ?>
	<header>
		<div id="header">
			<div class="hdTop margin0">
				<div class="row">
					<div class="flex-container">
						<div class="hdTopLeft">
							<p>
								<span>FOLLOW US: </span>
								<a href="<?php $this->info("fb_link") ?>" class="socialico" target="_blank">f</a>
								<a href="<?php $this->info("gp_link") ?>" class="socialico" target="_blank">g</a>
								<a href="<?php $this->info("tb_link") ?>" class="socialico" target="_blank">t</a>
								<a href="<?php $this->info("li_link") ?>" class="socialico" target="_blank">i</a>
								<a href="<?php $this->info("tt_link") ?>" class="socialico" target="_blank">l</a>
							</p>
						</div>
						<div class="hdTopMid">
							<p>
								<span>EMAIL US: </span>
								<?php $this->info(["email","mailto","email"]); ?>
							</p>
						</div>
						<div class="hdTopRight">
							<p>
								<span>CONTACT US:</span>
								<a href="tel:<?php $this->info("phone"); ?>"><?php $this->info("phone-display"); ?></a>
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="hdBot">
				<div class="row">
					<div class="desktop-nav">
						<nav>
							<ul class="nav-left">
								<li <?php $this->helpers->isActiveMenu("home"); ?>><a href="<?php echo URL ?>">Home</a></li>
								<li <?php $this->helpers->isActiveMenu("about"); ?>><a href="<?php echo URL ?>about#content">About Us</a></li>
								<li <?php $this->helpers->isActiveMenu("services"); ?>><a href="<?php echo URL ?>services#content">Services</a></li>
							</ul>
							<a href="<?php echo URL; ?>"><img src="public/images/common/mainLogo.png" class="mainLogo1" alt="<?php $this->info("company_name") ;?> Main Logo"></a>
							<ul class="nav-right">
								<li <?php $this->helpers->isActiveMenu("gallery"); ?>><a href="<?php echo URL ?>gallery#content">Gallery</a></li>
								<li <?php $this->helpers->isActiveMenu("reviews"); ?>><a href="<?php echo URL ?>reviews#content">Reviews</a></li>
								<li <?php $this->helpers->isActiveMenu("contact"); ?>><a href="<?php echo URL ?>contact#content">Contact Us</a></li>
							</ul>
						</nav>
					</div>
					<div class="mobile-nav">
						<nav id="nav-menu">
							<a href="#" id="pull"><strong>MENU</strong></a>
							<ul>
								<li <?php $this->helpers->isActiveMenu("home"); ?>><a href="<?php echo URL ?>">Home</a></li>
								<li <?php $this->helpers->isActiveMenu("about"); ?>><a href="<?php echo URL ?>about#content">About Us</a></li>
								<li <?php $this->helpers->isActiveMenu("services"); ?>><a href="<?php echo URL ?>services#content">Services</a></li>
								<li <?php $this->helpers->isActiveMenu("gallery"); ?>><a href="<?php echo URL ?>gallery#content">Gallery</a></li>
								<li <?php $this->helpers->isActiveMenu("reviews"); ?>><a href="<?php echo URL ?>reviews#content">Reviews</a></li>
								<li <?php $this->helpers->isActiveMenu("contact"); ?>><a href="<?php echo URL ?>contact#content">Contact Us</a></li>
							</ul>
						</nav>
						<a href="<?php echo URL; ?>"><img src="public/images/common/mainLogo.png" alt="<?php $this->info("company_name") ;?> Main Logo" class="mainLogo2"></a>
					</div>
				</div>
			</div>
		</div>
	</header>

	<?php if($view == "home"):?>
		<div id="banner">
			<div class="row">
				<h1>Distinctive Landscaping at its Finest</h1>
				<p class="desc">Offering professional services for customers who demand perfection</p>
				<a href="contact#content" class="button">Get a Free Quote</a>
				<p class="phone">
					<a href="tel:<?php $this->info("phone"); ?>"><?php $this->info("phone-display"); ?></a>
				</p>
			</div>
		</div>
	<?php endif; ?>
